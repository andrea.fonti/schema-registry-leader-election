#!/usr/bin/env bash

curl -X POST schema-registry-follower:8081/subjects/Platfrm-mstest001-ProvaDummy-value/versions -H 'accept: application/vnd.schemaregistry.v1+json' -H 'Content-Type: application/vnd.schemaregistry.v1+json' -d '{
   "schemaType": "AVRO",
   "references": [
   ],
   "schema": "{\"fields\": [{\"name\": \"name\",\"type\": \"string\" }, {\"name\": \"surname\",\"type\": \"string\"}],\"name\": \"Payload\",\"namespace\": \"com.enel.platform.common.model\",\"type\": \"record\"}"
 }'
