# Project description 

This repository is a reproducer for a missconfiguration of schema registry leader/follower relationships

## Run a properly configured cluster

Open a shell and run kafka

```
➜ docker-compose up kafka
```

wait to see 

```
kafka_1                     | [2021-12-17 09:19:33,241] INFO Kafka startTimeMs: 1639732773233 (org.apache.kafka.common.utils.AppInfoParser)
kafka_1                     | [2021-12-17 09:19:33,243] INFO [KafkaServer id=0] started (kafka.server.KafkaServer)
```

Open a shell and run the schema registry leader

```
➜ docker-compose up schema-registry-leader
```

wait to see

```
schema-registry-leader_1    | [2021-12-17 09:20:44,653] INFO Server started, listening for requests... (io.confluent.kafka.schemaregistry.rest.SchemaRegistryMain)
```

Leader election should have been completed and leader reports the configured hostname

```
schema-registry-leader_1    | [2021-12-17 09:20:43,411] INFO Finished rebalance with leader election result: Assignment{version=1, error=0, leader='sr-1-eb910336-fc14-49c5-bfc5-6368c2d6091b', leaderIdentity=version=1,host=schema-registry-leader,port=8081,scheme=http,leaderEligibility=true} (io.confluent.kafka.schemaregistry.leaderelector.kafka.KafkaGroupLeaderElector)
```

Opena a shell and run the schema registry follower

```
➜ docker-compose up schema-registry-follower
```

Wait to see

```
schema-registry-follower_1  | [2021-12-17 09:23:35,723] INFO Server started, listening for requests... (io.confluent.kafka.schemaregistry.rest.SchemaRegistryMain)
```

Follower should notice who is the leader and discover the hostname of the leader

```
schema-registry-follower_1  | [2021-12-17 09:23:34,544] INFO Finished rebalance with leader election result: Assignment{version=1, error=0, leader='sr-1-eb910336-fc14-49c5-bfc5-6368c2d6091b', leaderIdentity=version=1,host=schema-registry-leader,port=8081,scheme=http,leaderEligibility=true} (io.confluent.kafka.schemaregistry.leaderelector.kafka.KafkaGroupLeaderElector)
```

Run a write request targeted to the follower

```
➜ docker-compose exec schema-registry-follower bash /run-request-in-follower.sh
{"id":1}⏎
```

Follower will forward the request to master


*FOLLOWER LOGS*

```
schema-registry-follower_1  | [2021-12-17 09:25:25,935] INFO Registering new schema: subject Platfrm-mstest001-ProvaDummy-value, version null, id null, type AVRO, schema size 167 (io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource)
schema-registry-follower_1  | [2021-12-17 09:25:26,376] INFO 172.27.0.4 - - [17/Dec/2021:09:25:25 +0000] "POST /subjects/Platfrm-mstest001-ProvaDummy-value/versions HTTP/1.1" 200 8 "-" "curl/7.61.1" POSTsT (io.confluent.rest-utils.requests)
```

*MASTER LOGS*

```
schema-registry-leader_1    | [2021-12-17 09:25:26,211] INFO Registering new schema: subject Platfrm-mstest001-ProvaDummy-value, version 0, id -1, type null, schema size 157 (io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource)
schema-registry-leader_1    | [2021-12-17 09:25:26,258] INFO Wait to catch up until the offset at 3 (io.confluent.kafka.schemaregistry.storage.KafkaStore)
schema-registry-leader_1    | [2021-12-17 09:25:26,258] INFO Reached offset at 3 (io.confluent.kafka.schemaregistry.storage.KafkaStore)
schema-registry-leader_1    | [2021-12-17 09:25:26,278] INFO Wait to catch up until the offset at 4 (io.confluent.kafka.schemaregistry.storage.KafkaStore)
schema-registry-leader_1    | [2021-12-17 09:25:26,300] INFO Reached offset at 4 (io.confluent.kafka.schemaregistry.storage.KafkaStore)
schema-registry-leader_1    | [2021-12-17 09:25:26,340] INFO 172.27.0.4 - - [17/Dec/2021:09:25:26 +0000] "POST /subjects/Platfrm-mstest001-ProvaDummy-value/versions HTTP/1.1" 200 8 "-" "Java/11.0.13" POSTsT (io.confluent.rest-utils.requests)
```

## Run a miss-configured cluster

Open a shell and run kafka

```
➜ docker-compose -f docker-compose-wrong-hostnames.yaml up kafka
```


wait to see 

```
kafka_1                     | [2021-12-17 09:30:29,029] INFO Kafka startTimeMs: 1639733429023 (org.apache.kafka.common.utils.AppInfoParser)
kafka_1                     | [2021-12-17 09:30:29,032] INFO [KafkaServer id=0] started (kafka.server.KafkaServer)```

```
Open another shell and run the schema registry leader

```
➜ docker-compose -f docker-compose-wrong-hostnames.yaml  up schema-registry-leader
```

wait to see

```
schema-registry-leader_1    | [2021-12-17 09:31:36,807] INFO Server started, listening for requests... (io.confluent.kafka.schemaregistry.rest.SchemaRegistryMain)
```

Leader election should have been completed and leader reports the configured hostname (THAT NOW IS WRONG)

```
schema-registry-leader_1    | [2021-12-17 09:31:35,596] INFO Finished rebalance with leader election result: Assignment{version=1, error=0, leader='sr-1-42933c09-4212-4bea-b3b2-3e5f7f6d8e75', leaderIdentity=version=1,host=schema-registry-leader-wrong,port=8081,scheme=http,leaderEligibility=true} (io.confluent.kafka.schemaregistry.leaderelector.kafka.KafkaGroupLeaderElector)
```

Open another shell and run the schema registry follower

```
➜ docker-compose up schema-registry-follower
```

Wait to see

```
schema-registry-follower_1  | [2021-12-17 09:33:06,883] INFO Server started, listening for requests... (io.confluent.kafka.schemaregistry.rest.SchemaRegistryMain)
```

Follower should notice who is the leader and discover the hostname of the leader (WHICH IS WRONG)

```
schema-registry-follower_1  | [2021-12-17 09:33:05,647] INFO Finished rebalance with leader election result: Assignment{version=1, error=0, leader='sr-1-42933c09-4212-4bea-b3b2-3e5f7f6d8e75', leaderIdentity=version=1,host=schema-registry-leader-wrong,port=8081,scheme=http,leaderEligibility=true} (io.confluent.kafka.schemaregistry.leaderelector.kafka.KafkaGroupLeaderElector)
```

Run a write request targeted to the follower

```
➜ docker-compose exec schema-registry-follower bash /run-request-in-follower.sh
{"id":1}⏎
```

Replica is now not able to forward write requests to master

```
➜ docker-compose -f docker-compose-wrong-hostnames.yaml exec schema-registry-follower bash /run-request-in-follower.sh
{"error_code":50003,"message":"Error while forwarding register schema request to the leader io.confluent.kafka.schemaregistry.rest.exceptions.RestRequestForwardingException: Error while forwarding register schema request to the leader\nio.confluent.kafka.schemaregistry.rest.exceptions.RestRequestForwardingException: Error while forwarding register schema request to the leader\n\tat io.confluent.kafka.schemaregistry.rest.exceptions.Errors.requestForwardingFailedException(Errors.java:169)\n\tat io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource.register(SubjectVersionsResource.java:292)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:566)\n\tat org.glassfish.jersey.server.model.internal.ResourceMethodInvocationHandlerFactory.lambda$static$0(ResourceMethodInvocationHandlerFactory.java:52)\n\tat org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher$1.run(AbstractJavaResourceMethodDispatcher.java:124)\n\tat org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.invoke(AbstractJavaResourceMethodDispatcher.java:167)\n\tat org.glassfish.jersey.server.model.internal.JavaResourceMethodDispatcherProvider$VoidOutInvoker.doDispatch(JavaResourceMethodDispatcherProvider.java:159)\n\tat org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.dispatch(AbstractJavaResourceMethodDispatcher.java:79)\n\tat org.glassfish.jersey.server.model.ResourceMethodInvoker.invoke(ResourceMethodInvoker.java:475)\n\tat org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:397)\n\tat org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:81)\n\tat org.glassfish.jersey.server.ServerRuntime$1.run(ServerRuntime.java:255)\n\tat org.glassfish.jersey.internal.Errors$1.call(Errors.java:248)\n\tat org.glassfish.jersey.internal.Errors$1.call(Errors.java:244)\n\tat org.glassfish.jersey.internal.Errors.process(Errors.java:292)\n\tat org.glassfish.jersey.internal.Errors.process(Errors.java:274)\n\tat org.glassfish.jersey.internal.Errors.process(Errors.java:244)\n\tat org.glassfish.jersey.process.internal.RequestScope.runInScope(RequestScope.java:265)\n\tat org.glassfish.jersey.server.ServerRuntime.process(ServerRuntime.java:234)\n\tat org.glassfish.jersey.server.ApplicationHandler.handle(ApplicationHandler.java:680)\n\tat org.glassfish.jersey.servlet.WebComponent.serviceImpl(WebComponent.java:394)\n\tat org.glassfish.jersey.servlet.ServletContainer.serviceImpl(ServletContainer.java:386)\n\tat org.glassfish.jersey.servlet.ServletContainer.doFilter(ServletContainer.java:561)\n\tat org.glassfish.jersey.servlet.ServletContainer.doFilter(ServletContainer.java:502)\n\tat org.glassfish.jersey.servlet.ServletContainer.doFilter(ServletContainer.java:439)\n\tat org.eclipse.jetty.servlet.FilterHolder.doFilter(FilterHolder.java:193)\n\tat org.eclipse.jetty.servlet.ServletHandler$Chain.doFilter(ServletHandler.java:1601)\n\tat org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:548)\n\tat org.eclipse.jetty.server.handler.ScopedHandler.nextHandle(ScopedHandler.java:233)\n\tat org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:1624)\n\tat org.eclipse.jetty.server.handler.ScopedHandler.nextHandle(ScopedHandler.java:233)\n\tat org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1434)\n\tat org.eclipse.jetty.server.handler.ScopedHandler.nextScope(ScopedHandler.java:188)\n\tat org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:501)\n\tat org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:1594)\n\tat org.eclipse.jetty.server.handler.ScopedHandler.nextScope(ScopedHandler.java:186)\n\tat org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:1349)\n\tat org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:141)\n\tat org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:146)\n\tat org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:146)\n\tat org.eclipse.jetty.server.handler.StatisticsHandler.handle(StatisticsHandler.java:179)\n\tat org.eclipse.jetty.server.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:234)\n\tat org.eclipse.jetty.server.handler.gzip.GzipHandler.handle(GzipHandler.java:763)\n\tat org.eclipse.jetty.server.handler.HandlerWrapper.handle(HandlerWrapper.java:127)\n\tat org.eclipse.jetty.server.Server.handle(Server.java:516)\n\tat org.eclipse.jetty.server.HttpChannel.lambda$handle$1(HttpChannel.java:388)\n\tat org.eclipse.jetty.server.HttpChannel.dispatch(HttpChannel.java:633)\n\tat org.eclipse.jetty.server.HttpChannel.handle(HttpChannel.java:380)\n\tat org.eclipse.jetty.server.HttpConnection.onFillable(HttpConnection.java:277)\n\tat org.eclipse.jetty.io.AbstractConnection$ReadCallback.succeeded(AbstractConnection.java:311)\n\tat org.eclipse.jetty.io.FillInterest.fillable(FillInterest.java:105)\n\tat org.eclipse.jetty.io.ChannelEndPoint$1.run(ChannelEndPoint.java:104)\n\tat org.eclipse.jetty.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:883)\n\tat org.eclipse.jetty.util.thread.QueuedThreadPool$Runner.run(QueuedThreadPool.java:1034)\n\tat java.base/java.lang.Thread.run(Thread.java:829)\nCaused by: io.confluent.kafka.schemaregistry.exceptions.SchemaRegistryRequestForwardingException: Unexpected error while forwarding the registering schema request {version=0, id=-1, schemaType=AVRO,references=[],schema={\"type\":\"record\",\"name\":\"Payload\",\"namespace\":\"com.enel.platform.common.model\",\"fields\":[{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"surname\",\"type\":\"string\"}]}} to [http://schema-registry-leader-wrong:8081]\n\tat io.confluent.kafka.schemaregistry.storage.KafkaSchemaRegistry.forwardRegisterRequestToLeader(KafkaSchemaRegistry.java:832)\n\tat io.confluent.kafka.schemaregistry.storage.KafkaSchemaRegistry.registerOrForward(KafkaSchemaRegistry.java:599)\n\tat io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource.register(SubjectVersionsResource.java:279)\n\t... 56 more\nCaused by: java.net.UnknownHostException: schema-registry-leader-wrong\n\tat java.base/java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:220)\n\tat java.base/java.net.Socket.connect(Socket.java:609)\n\tat java.base/sun.net.NetworkClient.doConnect(NetworkClient.java:177)\n\tat java.base/sun.net.www.http.HttpClient.openServer(HttpClient.java:474)\n\tat java.base/sun.net.www.http.HttpClient.openServer(HttpClient.java:569)\n\tat java.base/sun.net.www.http.HttpClient.<init>(HttpClient.java:242)\n\tat java.base/sun.net.www.http.HttpClient.New(HttpClient.java:341)\n\tat java.base/sun.net.www.http.HttpClient.New(HttpClient.java:362)\n\tat java.base/sun.net.www.protocol.http.HttpURLConnection.getNewHttpClient(HttpURLConnection.java:1253)\n\tat java.base/sun.net.www.protocol.http.HttpURLConnection.plainConnect0(HttpURLConnection.java:1187)\n\tat java.base/sun.net.www.protocol.http.HttpURLConnection.plainConnect(HttpURLConnection.java:1081)\n\tat java.base/sun.net.www.protocol.http.HttpURLConnection.connect(HttpURLConnection.java:1015)\n\tat java.base/sun.net.www.protocol.http.HttpURLConnection.getOutputStream0(HttpURLConnection.java:1367)\n\tat java.base/sun.net.www.protocol.http.HttpURLConnection.getOutputStream(HttpURLConnection.java:1342)\n\tat io.confluent.kafka.schemaregistry.client.rest.RestService.sendHttpRequest(RestService.java:269)\n\tat io.confluent.kafka.schemaregistry.client.rest.RestService.httpRequest(RestService.java:368)\n\tat io.confluent.kafka.schemaregistry.client.rest.RestService.registerSchema(RestService.java:511)\n\tat io.confluent.kafka.schemaregistry.storage.KafkaSchemaRegistry.forwardRegisterRequestToLeader(KafkaSchemaRegistry.java:828)\n\t... 58 more\n"}⏎
```


FOLLOWER LOGS

```
schema-registry-follower_1  | [2021-12-17 09:35:30,675] ERROR Request Failed with exception  (io.confluent.rest.exceptions.DebuggableExceptionMapper)
schema-registry-follower_1  | io.confluent.kafka.schemaregistry.rest.exceptions.RestRequestForwardingException: Error while forwarding register schema request to the leader
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.rest.exceptions.Errors.requestForwardingFailedException(Errors.java:169)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource.register(SubjectVersionsResource.java:292)
schema-registry-follower_1  | 	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
schema-registry-follower_1  | 	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
schema-registry-follower_1  | 	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
schema-registry-follower_1  | 	at java.base/java.lang.reflect.Method.invoke(Method.java:566)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.internal.ResourceMethodInvocationHandlerFactory.lambda$static$0(ResourceMethodInvocationHandlerFactory.java:52)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher$1.run(AbstractJavaResourceMethodDispatcher.java:124)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.invoke(AbstractJavaResourceMethodDispatcher.java:167)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.internal.JavaResourceMethodDispatcherProvider$VoidOutInvoker.doDispatch(JavaResourceMethodDispatcherProvider.java:159)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.internal.AbstractJavaResourceMethodDispatcher.dispatch(AbstractJavaResourceMethodDispatcher.java:79)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.ResourceMethodInvoker.invoke(ResourceMethodInvoker.java:475)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:397)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.model.ResourceMethodInvoker.apply(ResourceMethodInvoker.java:81)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.ServerRuntime$1.run(ServerRuntime.java:255)
schema-registry-follower_1  | 	at org.glassfish.jersey.internal.Errors$1.call(Errors.java:248)
schema-registry-follower_1  | 	at org.glassfish.jersey.internal.Errors$1.call(Errors.java:244)
schema-registry-follower_1  | 	at org.glassfish.jersey.internal.Errors.process(Errors.java:292)
schema-registry-follower_1  | 	at org.glassfish.jersey.internal.Errors.process(Errors.java:274)
schema-registry-follower_1  | 	at org.glassfish.jersey.internal.Errors.process(Errors.java:244)
schema-registry-follower_1  | 	at org.glassfish.jersey.process.internal.RequestScope.runInScope(RequestScope.java:265)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.ServerRuntime.process(ServerRuntime.java:234)
schema-registry-follower_1  | 	at org.glassfish.jersey.server.ApplicationHandler.handle(ApplicationHandler.java:680)
schema-registry-follower_1  | 	at org.glassfish.jersey.servlet.WebComponent.serviceImpl(WebComponent.java:394)
schema-registry-follower_1  | 	at org.glassfish.jersey.servlet.ServletContainer.serviceImpl(ServletContainer.java:386)
schema-registry-follower_1  | 	at org.glassfish.jersey.servlet.ServletContainer.doFilter(ServletContainer.java:561)
schema-registry-follower_1  | 	at org.glassfish.jersey.servlet.ServletContainer.doFilter(ServletContainer.java:502)
schema-registry-follower_1  | 	at org.glassfish.jersey.servlet.ServletContainer.doFilter(ServletContainer.java:439)
schema-registry-follower_1  | 	at org.eclipse.jetty.servlet.FilterHolder.doFilter(FilterHolder.java:193)
schema-registry-follower_1  | 	at org.eclipse.jetty.servlet.ServletHandler$Chain.doFilter(ServletHandler.java:1601)
schema-registry-follower_1  | 	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:548)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ScopedHandler.nextHandle(ScopedHandler.java:233)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:1624)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ScopedHandler.nextHandle(ScopedHandler.java:233)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1434)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ScopedHandler.nextScope(ScopedHandler.java:188)
schema-registry-follower_1  | 	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:501)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:1594)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ScopedHandler.nextScope(ScopedHandler.java:186)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:1349)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:141)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:146)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:146)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.StatisticsHandler.handle(StatisticsHandler.java:179)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:234)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.gzip.GzipHandler.handle(GzipHandler.java:763)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.handler.HandlerWrapper.handle(HandlerWrapper.java:127)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.Server.handle(Server.java:516)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.HttpChannel.lambda$handle$1(HttpChannel.java:388)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.HttpChannel.dispatch(HttpChannel.java:633)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.HttpChannel.handle(HttpChannel.java:380)
schema-registry-follower_1  | 	at org.eclipse.jetty.server.HttpConnection.onFillable(HttpConnection.java:277)
schema-registry-follower_1  | 	at org.eclipse.jetty.io.AbstractConnection$ReadCallback.succeeded(AbstractConnection.java:311)
schema-registry-follower_1  | 	at org.eclipse.jetty.io.FillInterest.fillable(FillInterest.java:105)
schema-registry-follower_1  | 	at org.eclipse.jetty.io.ChannelEndPoint$1.run(ChannelEndPoint.java:104)
schema-registry-follower_1  | 	at org.eclipse.jetty.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:883)
schema-registry-follower_1  | 	at org.eclipse.jetty.util.thread.QueuedThreadPool$Runner.run(QueuedThreadPool.java:1034)
schema-registry-follower_1  | 	at java.base/java.lang.Thread.run(Thread.java:829)
schema-registry-follower_1  | Caused by: io.confluent.kafka.schemaregistry.exceptions.SchemaRegistryRequestForwardingException: Unexpected error while forwarding the registering schema request {version=0, id=-1, schemaType=AVRO,references=[],schema={"type":"record","name":"Payload","namespace":"com.enel.platform.common.model","fields":[{"name":"name","type":"string"},{"name":"surname","type":"string"}]}} to [http://schema-registry-leader-wrong:8081]
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.storage.KafkaSchemaRegistry.forwardRegisterRequestToLeader(KafkaSchemaRegistry.java:832)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.storage.KafkaSchemaRegistry.registerOrForward(KafkaSchemaRegistry.java:599)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource.register(SubjectVersionsResource.java:279)
schema-registry-follower_1  | 	... 56 more
schema-registry-follower_1  | Caused by: java.net.UnknownHostException: schema-registry-leader-wrong
schema-registry-follower_1  | 	at java.base/java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:220)
schema-registry-follower_1  | 	at java.base/java.net.Socket.connect(Socket.java:609)
schema-registry-follower_1  | 	at java.base/sun.net.NetworkClient.doConnect(NetworkClient.java:177)
schema-registry-follower_1  | 	at java.base/sun.net.www.http.HttpClient.openServer(HttpClient.java:474)
schema-registry-follower_1  | 	at java.base/sun.net.www.http.HttpClient.openServer(HttpClient.java:569)
schema-registry-follower_1  | 	at java.base/sun.net.www.http.HttpClient.<init>(HttpClient.java:242)
schema-registry-follower_1  | 	at java.base/sun.net.www.http.HttpClient.New(HttpClient.java:341)
schema-registry-follower_1  | 	at java.base/sun.net.www.http.HttpClient.New(HttpClient.java:362)
schema-registry-follower_1  | 	at java.base/sun.net.www.protocol.http.HttpURLConnection.getNewHttpClient(HttpURLConnection.java:1253)
schema-registry-follower_1  | 	at java.base/sun.net.www.protocol.http.HttpURLConnection.plainConnect0(HttpURLConnection.java:1187)
schema-registry-follower_1  | 	at java.base/sun.net.www.protocol.http.HttpURLConnection.plainConnect(HttpURLConnection.java:1081)
schema-registry-follower_1  | 	at java.base/sun.net.www.protocol.http.HttpURLConnection.connect(HttpURLConnection.java:1015)
schema-registry-follower_1  | 	at java.base/sun.net.www.protocol.http.HttpURLConnection.getOutputStream0(HttpURLConnection.java:1367)
schema-registry-follower_1  | 	at java.base/sun.net.www.protocol.http.HttpURLConnection.getOutputStream(HttpURLConnection.java:1342)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.client.rest.RestService.sendHttpRequest(RestService.java:269)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.client.rest.RestService.httpRequest(RestService.java:368)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.client.rest.RestService.registerSchema(RestService.java:511)
schema-registry-follower_1  | 	at io.confluent.kafka.schemaregistry.storage.KafkaSchemaRegistry.forwardRegisterRequestToLeader(KafkaSchemaRegistry.java:828)
schema-registry-follower_1  | 	... 58 more
```